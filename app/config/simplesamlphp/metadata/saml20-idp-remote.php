<?php
/**
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote 
 */

$metadata['http://localhost:8080/saml2/idp/metadata.php'] = array (
  'metadata-set' => 'saml20-idp-remote',
  'entityid' => 'http://localhost:8080/saml2/idp/metadata.php',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'http://localhost:8080/saml2/idp/SSOService.php',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'http://localhost:8080/saml2/idp/SingleLogoutService.php',
    ),
  ),
  'certData' => 'MIIDzzCCAregAwIBAgIJAJ/PV4WoTo4FMA0GCSqGSIb3DQEBCwUAMH4xCzAJBgNVBAYTAkVTMQ8wDQYDVQQIDAZNYWRyaWQxDzANBgNVBAcMBk1hZHJpZDEOMAwGA1UECgwFaW50ZWYxDjAMBgNVBAsMBWludGVmMQ4wDAYDVQQDDAVpbnRlZjEdMBsGCSqGSIb3DQEJARYOaW50ZWZAaW50ZWYuZXMwHhcNMTYwNTA2MDk0MDA0WhcNMjYwNTA2MDk0MDA0WjB+MQswCQYDVQQGEwJFUzEPMA0GA1UECAwGTWFkcmlkMQ8wDQYDVQQHDAZNYWRyaWQxDjAMBgNVBAoMBWludGVmMQ4wDAYDVQQLDAVpbnRlZjEOMAwGA1UEAwwFaW50ZWYxHTAbBgkqhkiG9w0BCQEWDmludGVmQGludGVmLmVzMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2UpI2badKcoHrV/FZ6OgrzFxAu6D2hb3ywXYGMfBIxySxiNY2HG9y9UgyxuoYogTEhf9lxAPYRSXJBeiK2XOabSXADUvpMYYrrNDdqe4klCCZFBPIRA4lFMlidcwkfUK7MMVnCkYQAF0TYlp7OsygY0ZXQ81OVVVn71pxLeN0y8oljjT+W9QF0MtC9DH2stRauGWZbKxOEw78AiKvekwQFuPWDL9jSOR+YbVvWfQzftkhNEWTEwKA8It6AHkSUGfPFRzldtMa4KOSX+oZjwVPMAGJh0SzgFdAe3O2uct4TbszdrHWtXDzWGWrAgMpQ+dgPdGLfIax+kTgB7k06PhxQIDAQABo1AwTjAdBgNVHQ4EFgQUOG5V6Q/QkSPyQSHr8eSynjVVJiwwHwYDVR0jBBgwFoAUOG5V6Q/QkSPyQSHr8eSynjVVJiwwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAz87caUzPlMPQ8TlPuiKbGMseHeZUQyn5JJHe47qeGTHtjEUuYpqY4gpzAkGmeroanZbCYW0R1D1WrEkpzOYO/ze3apDqwbfwrt2m6s9sOeFBMNAY1+CZOP+s6bhcr73LF2daVi6HDv2NF15OPVPc5pYBjkMEEMOctWl7MYwqPY+Xmc2xB9txU9o+YTbpNlBQxkkOAV5+7M4eAWyJWhKSxqj4XxZgS1YEyl5zMOPMu448K9zveD0WXxZ1mPwl3q3Bq6zLX/9mgGCYWGaKicKuqyW3cRpztWURktbzf+1ddNTX49FHxIyxrS1SWVofozktMrZVF24wdxZtN0YNjYctUg==',
  'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
);
