simplesamlphpsf2sp
==================

Este proyeto-tutorial tiene como finalidad mostrar una posible manera para 
proteger una aplicación Sf2 con SAML. Utilizaremos simpleSAMLphp como Idp y
el plugin https://github.com/hslavich/SimplesamlphpBundle, para convertir
el proyecto Sf2 en SP de SAML. Este plugin, como indica su nombre, también
utiliza el proyecto simpleSAMLphp (https://simplesamlphp.org/).


# Instalación de un Idp

En primer lugar tenemos que disponer de un Idp. Lo podemos instalar y configurar
rápidamente de la siguiente manera.

Descargamos de https://simplesamlphp.org/download la última versión de 
simpleSAMLphp. Para el desarrollo de este tutorial vamos a servir el Idp a 
través del servidor incorporado en PHP. Para ello nos vamos al directorio 
raíz del simpleSAMLphp y ejecutamos:

    php -S localhost:8080 -t www

Y ahora tenemos que configurar el Idp para que funcione correctamente. Una 
configuración mínima se hace de la siguiente manera:

En el archivo config/config.php, cambiamos:

    'baseurlpath' => '/',
    'enable.saml20-idp' => true,

En el archivo config/authsources.php descomentamos la fuente de autenticación
'example-userpass'. De ahí obtendremos los usuarios en este tutorial.

En el archivo metadata/saml20-idp-hosted.php, nos aseguramos que:

    'auth' => 'example-userpass',


Hay que meter los certificados que están en el directorio 'certificadosParaIdp' 
de este proyecto en el directorio 'cert' del Idp.

Y por último en archivo metadata/saml20-sp-remote debe tener el siguiente 
contenido:

    <?php
    $metadata['http://localhost:8000/simplesaml/module.php/saml/sp/metadata.php/default-sp'] = array (
      'SingleLogoutService' => 
      array (
        0 => 
        array (
          'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
          'Location' => 'http://localhost:8000/simplesaml/module.php/saml/sp/saml2-logout.php/default-sp',
        ),
      ),
      'AssertionConsumerService' => 
      array (
        0 => 
        array (
          'index' => 0,
          'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
          'Location' => 'http://localhost:8000/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp',
        ),
        1 => 
        array (
          'index' => 1,
          'Binding' => 'urn:oasis:names:tc:SAML:1.0:profiles:browser-post',
          'Location' => 'http://localhost:8000/simplesaml/module.php/saml/sp/saml1-acs.php/default-sp',
        ),
        2 => 
        array (
          'index' => 2,
          'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact',
          'Location' => 'http://localhost:8000/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp',
        ),
        3 => 
        array (
          'index' => 3,
          'Binding' => 'urn:oasis:names:tc:SAML:1.0:profiles:artifact-01',
          'Location' => 'http://localhost:8000/simplesaml/module.php/saml/sp/saml1-acs.php/default-sp/artifact',
        ),
      ),
    );

Nota: Esto es algo que realmente se extrae del SP una vez instalado. Aunque aún 
no lo hemos instalados, ya sabemos su valor por que la forma en que realizaremos
la instalación del SP dará lugar a estos valores.

Y el Idp ya podemos probarlo en http://localhost:8080. Nos vamos a
Autenticación -> Probar las fuentes para la autentificación ya configuradas ->
example-userpass, y probamos la fuente en cuestión.

Nota: Para facilitar estos pasos hemos incluido en la raíz del proyecto una
instancia ya configurada de simplesamlphp (simplesamlphp-1.14.3.tgz). Basta descomprimirla
y lanzar:

	php -S localhost:8080 -t www

desde el raíz de esta instancia.


# Convertir el proyecto Symfony2 en SP de SAML

Este proyecto ya está configurado para ser SP de SAML. Aquí vamos a contar como
se ha hecho.

En primer lugar, con composer, se añade el https://github.com/hslavich/SimplesamlphpBundle.

    composer.phar require hslavich/simplesamlphp-bundle

Activamos el bundle añadiendo al app/AppKernel.php

    new Hslavich\SimplesamlphpBundle\HslavichSimplesamlphpBundle(),

Añadimos al app/config/config.yml

    hslavich_simplesamlphp:
        # Service provider name
        sp: default-sp
        authentication_attribute: sAMAccountName   # o el que sea

Y en ese mismo archivo añadimos como tipo de sesión  session.storage.php_bridge:

    session:
        # handler_id set to null will use default session handler from php.ini
        storage_id: session.storage.php_bridge
        handler_id:  ~


Creamos la siguiente entidad usuario (AppBundle\Entity\User):

    <?php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Security\Core\User\UserInterface;
    use Symfony\Component\Security\Core\User\EquatableInterface;

    /**
     * User
     *
     * @ORM\Table(name="my_user")
     * @ORM\Entity()
     */
    class User implements UserInterface, EquatableInterface {

        /**
         * @var string
         *
         * @ORM\Column(name="username", type="string", length=255, unique=true)
         */
        private $username;

        /**
         * @var string
         *
         * @ORM\Column(name="password", type="string", length=255)
         */
        private $password;

        /**
         * @var string
         *
         * @ORM\Column(name="salt", type="string", length=255)
         */
        private $salt;

        /**
         * @var string
         *
         * @ORM\Column(name="roles", type="string")
         */
        private $roles;

        public function __construct($username, $password, $salt,  $roles) {
            $this->username = $username;
            $this->password = $password;
            $this->salt = $salt;
            $this->roles = $roles;
        }

        /**
         * @var int
         *
         * @ORM\Column(name="id", type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        public function getId() {
            return $this->id;
        }

        public function eraseCredentials() {

        }

        public function getPassword() {
            return $this->password;
        }

        public function setPassword($password) {
            $this->password = $password;
        }

        public function getRoles() {
            return json_decode($this->roles, true);
        }

        public function setRoles($roles) {
            $this->roles = $roles;
        }

        public function getSalt() {
            return $this->salt;
        }

        public function getUsername() {
            return $this->username;
        }

        public function setUsername($username) {
            $this->username = $username;
        }

        public function isEqualTo(UserInterface $user) {
            if (!$user instanceof User) {
                return false;
            }

            if ($this->password !== $user->getPassword()) {
                return false;
            }

            if ($this->salt !== $user->getSalt()) {
                return false;
            }

            if ($this->username !== $user->getUsername()) {
                return false;
            }

            return true;
        }

    }

Creamos un user provider en AppBundle\Security\User\UserProvider:

    <?php

    namespace AppBundle\Security\User;

    use Symfony\Component\Security\Core\User\UserProviderInterface;
    use Symfony\Component\Security\Core\User\UserInterface;
    use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
    use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
    use AppBundle\Entity\User;

    class UserProvider implements UserProviderInterface {

        private $em;
        private $simpleSAMLAuth;

        public function __construct(\Doctrine\ORM\EntityManager $em, $simpleSAMLAuth) {
            $this->em = $em;
            $this->simpleSAMLAuth = $simpleSAMLAuth;
        }

        public function loadUserByUsername($username) {

            $user = $this->em->getRepository('AppBundle:User')->findOneBy([
                'username' => $username
                    ]
            );


            if(!$user instanceof User){

                $user = new User($username, 'saml_user', '', json_encode(['ROLES_USER']));

                $this->em->persist($user);

                $this->em->flush();
            }

            return $user;
        }

        public function refreshUser(UserInterface $user) {
            if (!$user instanceof User) {
                throw new UnsupportedUserException(
                    sprintf('Instances of "%s" are not supported.', get_class($user))
                );
            }

            return $this->loadUserByUsername($user->getUsername());
        }

        public function supportsClass($class) {
            return $class === 'AppBundle\Entity\User';
        }

    }

El funcionamiento de este user provider es muy sencillo, busca en la base de 
datos un usuario con el username que el SAML le ha devuelto. Si está, lo 
devuelve como entidad User, si no lo crea en la base de dato y lo devuelve
como entidad User.

Creamos un logout handler:

    <?php
    namespace AppBundle\Security\Http\Logout;

    use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface; 
    use Symfony\Component\HttpFoundation\Request; 
    use Symfony\Component\HttpFoundation\RedirectResponse; 
    use Symfony\Component\Routing\Router;

    class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
    {
        protected $auth;
        protected $router;
        protected $logoutTarget;


        public function __construct($auth, Router $router, $logoutTarget)
        {
            $this->auth = $auth;
            $this->router = $router;
            $this->logoutTarget = $logoutTarget;
        }

        public function onLogoutSuccess(Request $request)
        {
            $returnTo = $request->headers->get('referer', '/');
            $request->getSession()->invalidate();

            //ldd($this->router->generate('public', array(), true ));

            $this->auth->logout($this->router->generate($this->logoutTarget, array(), true));

            //ldd($request);

            return new RedirectResponse($this->auth->getLogoutURL($returnTo));

        }
    }

Ya tenemos todos los archivos necesarios. Ahora vamos a la configuración.

En app/config debemos añadir el directorio simplesamlphp con la siguiente
estructura:

     simplesamlphp
     |- cert
     |- config
     |  |- authsources.php
     |  `- config.php
     `- metadata
       `- saml20-idp-remote

Estos fichero (míralos en su sitio) se deben copiar dentro del simplesamlphp que
viene incorporado en el proyecto cuando se añade el plugin de SAML. Para 
copiarlos:

    app/console simplesamlphp:config


En el directorio web del proyecto Sf2 crear el siguiente enlace simbolico:

    ln -s ../vendor/simplesamlphp/simplesamlphp/www/ simplesaml

Esto en producción se sustituiría por un alias de apache.

El fichero app/config/security.yml:

    security:
        providers:
            simplesaml:
                id: ysaml_user_provider

        firewalls:
            dev:
                pattern: ^/(_(profiler|wdt)|css|images|js)/
                security: false

            saml:
                pattern:    ^/saml
                anonymous: true
                stateless:  true
                simple_preauth:
                    authenticator: simplesamlphp.authenticator
                    provider: simplesaml
                logout:
                    path:   /saml/logout
                    target: %logout_target%                
                    success_handler: ysimplesamlphp.logout_handler

El fichero app/config/services.yml

    # Learn more about services, parameters and containers at
    # http://symfony.com/doc/current/book/service_container.html
    parameters:
    #    parameter_name: value

    services:
        ysaml_user_provider:
            class: AppBundle\Security\User\UserProvider
            arguments: [@doctrine.orm.entity_manager, @simplesamlphp.auth]

        ysimplesamlphp.logout_handler:
            class: AppBundle\Security\Http\Logout\LogoutSuccessHandler
            arguments: [ '@simplesamlphp.auth', '@router' , %logout_target%]    

El fichero app/config/parameters.yml

    parameters:
        database_host: 127.0.0.1
        database_port: null
        database_name: samlsf2
        database_user: root
        database_password: root
        mailer_transport: smtp
        mailer_host: 127.0.0.1
        mailer_user: null
        mailer_password: null
        secret: 587bf5a962cd563487532d994360aa297f3135b6
        logout_target: homepage

Y ya queda todo configurado. 

Debemos crear la base de datos y el schema:

app/console doctrine:database:create
app/console doctrine:schema:update

Ahora metemos algunas acciones para probar el 
invento. En AppBundle/Controllers/DefaultController.php:

    <?php

    namespace AppBundle\Controller;

    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\Request;

    class DefaultController extends Controller
    {
        /**
         * @Route("/", name="homepage")
         */
        public function indexAction(Request $request)
        {

            // replace this example code with whatever you need
            return $this->render('default/index.html.twig', array(            
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            ));
        }

        /**
         * Ruta protegida
         * @Route("/saml", name="saml")
         * @return type
         */
        public function samlAction(){

            $simplesamlAuth = $this->get('simplesamlphp.auth');


            $samlAttributes = $simplesamlAuth->getAttributes();

            echo '<pre>';
            print_r($samlAttributes);
            echo '</pre>';    

            return $this->render('default/index.html.twig', array(            
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            ));
        }

        /**
         * @Route("/saml/logout", name="logout")
         */
        public function logoutAction(){}
    }

La primera es una ruta pública, la segunda es privada y la tercera es la de 
logout. 

Arrancamos el servidor de Sf2:

app/console server:run

Y ya podemos probar la apli en:

http://localhost:8000

http://localhost:8000/saml (protegida por saml)

http://localhosy:8000/saml/logout

Para entrar se pueden usar los siguientes usuarios:

student, studentpass
employee, employeepass