<?php

namespace AppBundle\Security\User;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use AppBundle\Entity\User;

class UserProvider implements UserProviderInterface {

    private $em;
    private $simpleSAMLAuth;

    public function __construct(\Doctrine\ORM\EntityManager $em, $simpleSAMLAuth) {
        $this->em = $em;
        $this->simpleSAMLAuth = $simpleSAMLAuth;
    }

    public function loadUserByUsername($username) {

//        print_r($username);
//        
//        print_r($this->simpleSAMLAuth->getAttributes());
//        
//        print_r($this->em->getRepository('AppBundle:User'));
//        
//        exit;
        
        $user = $this->em->getRepository('AppBundle:User')->findOneBy([
            'username' => $username
                ]
        );
               
        
        if(!$user instanceof User){
                   
            $user = new User($username, 'saml_user', '', json_encode(['ROLES_USER']));
                                               
            $this->em->persist($user);
            
            $this->em->flush();
        }

        return $user;
    }

    public function refreshUser(UserInterface $user) {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class) {
        return $class === 'AppBundle\Entity\User';
    }

}
