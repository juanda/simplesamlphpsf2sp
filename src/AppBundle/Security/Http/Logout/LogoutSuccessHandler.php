<?php

namespace AppBundle\Security\Http\Logout;

use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

class LogoutSuccessHandler implements LogoutSuccessHandlerInterface
{
    protected $auth;
    protected $router;
    protected $logoutTarget;


    public function __construct($auth, Router $router, $logoutTarget)
    {
        $this->auth = $auth;
        $this->router = $router;
        $this->logoutTarget = $logoutTarget;
    }

    public function onLogoutSuccess(Request $request)
    {
        $returnTo = $request->headers->get('referer', '/');
        $request->getSession()->invalidate();
        
        //ldd($this->router->generate('public', array(), true ));
        
        $this->auth->logout($this->router->generate($this->logoutTarget, array(), true));
        
        //ldd($request);
        
        return new RedirectResponse($this->auth->getLogoutURL($returnTo));
               
    }
}


